from django.shortcuts import redirect, render, redirect
from django.db import connection
import cx_Oracle
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from .forms import *
from .utils import * 
from datetime import date
from django.shortcuts import render, redirect, get_object_or_404
from .models import  *
from usuarios.forms import FormularioUsuario, FormularioUsuarioCompleto,AgregadoAdminForms
from usuarios.models import Usuario
from django.contrib.messages.api import success
import re

# Create your views here.

def home(reques):
    return render(reques, 'core/index.html')

def modulos(reques):
    return render(reques, 'core/modulos.html')

###--------------------------------logins---------------------------------------
#Usuario
def usuario(request,id):
    persona = get_object_or_404(Usuario, id=id)
    data = {
        'form':FormularioUsuario(instance=persona),
    }
    if request.method == 'POST':
        formulario = FormularioUsuario(data=request.POST, instance=persona)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, " Datos modificados correctamente ")
        else:
            data["form"] = formulario
            messages.warning(request, "ERROR: Los datos no fueron actualizados")

    return render(request,'core/persona/modificar.html',data)

def nuevo_usuario(request):
    data = {
        'form':AgregadoAdminForms(),
    }
    if request.method == 'POST':
        formulario = AgregadoAdminForms(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, " Usuario Registrado correctamente ")
        else:
            data["form"] = formulario
            messages.warning(request, "ERROR: el usuario no fue registrado")
    return render(request, 'core/persona/agregar.html',data)
    
def modificar_usuario(request,id):
    persona = get_object_or_404(Usuario, id=id)
    data = {
        'form':FormularioUsuarioCompleto(instance=persona),
    }
    if request.method == 'POST':
        formulario = FormularioUsuarioCompleto(data=request.POST, instance=persona)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, " Datos modificados correctamente ")
        else:
            data["form"] = formulario
            messages.warning(request, "ERROR: Los datos no fueron actualizados")
    return render(request, 'core/persona/modificar.html',data)

def listar_usuario(request):
    persona = Usuario.objects.all()
    data = {
        'usuario':persona,
    }
    return render(request, 'core/persona/listar.html',data)

def eliminar_usuario(request,id):
    usuari = get_object_or_404(Usuario, id=id)
    usuari.delete()
    messages,success(request, "Usuario eliminadao correctamente")
    return redirect(to="listar_usuario")

def registrate(request):
    data = {
        'form':registroform()
    }
    if request.method == 'POST':
        formulario = registroform(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(username=formulario.cleaned_data["username"],password=formulario.cleaned_data["password"])
            auth_login(request, user)
            messages.success(request, "Registro exitoso")
            return redirect(to="home")
        data['from'] = formulario
    return render(request, 'core/registration/registrate.html', data)

def habitaciones(request):
    return render(request, 'core/habitaciones.html')

def ordencompra(request):
    return render(request, 'core/ordencompra.html')

def promociones(request):
    return render(request, 'core/promociones.html')

def factura(request, id):
    data = {
        'numerito':int(id),
        'facturas':listar_factura,
    }
    pdf = render_to_pdf('core/editar/factura/factura.html', data)
    return HttpResponse(pdf, content_type='application/pdf')

def listarfacturas(request):
    data = {
        'facturas':listar_factura,
    }
    return render(request, 'core/listarfacturas.html', data)

def crearFactura(request, id):
    data = {
        'numerito':int(id),
        'huespedes':listar_huesped(),
    }
    if request.method == 'POST':
        precio_hab = int(request.POST.get('precio_total'))
        iva_19 = int(request.POST.get('precio_total'))*0.19
        precio_totale = round(int(request.POST.get('precio_total'))*1.19)
        nombre_empresa = request.POST.get('nom_empresa')
        nombre_cliente = request.POST.get('nom_cliente')
        habitacione = int(request.POST.get('habitacion'))
        rut_cliente = request.POST.get('cliente')
        fecha = date.today()
        creador = request.POST.get('creador')
        
        salida = agregar_factura(precio_hab,iva_19,precio_totale,nombre_empresa,nombre_cliente,habitacione,rut_cliente,fecha,creador)
        if salida == 1:
            data['mensaje'] = 'registrado correctamente'
        else:
            data['mensaje'] = 'No se a podido registrar'

    return render(request, 'core/editar/factura/crearFactura.html', data)

def registrohuesped(request):
    data = {
        'huespedes':listar_huesped(),
    }

    return render(request, 'core/registrohuesped.html', data)

def crearHuesped(request):
    data = {
        'usuarios': listar_users(),
        'habitaciones': listar_habitacion(),
        'empresas': listar_empresa(),
    }

    if request.method == 'POST':
        usuario = int(request.POST.get('id_usuarios'))
        empresa = int(request.POST.get('id_empresas'))
        habitacion = int(request.POST.get('id_habitaciones'))
        salida = agregar_huesped(usuario,empresa,habitacion)
        
        if salida == 1:
            data['mensaje'] = 'registrado correctamente'
        else:
            data['mensaje'] = 'No se a podido registrar'

    return render(request, 'core/editar/huesped/crearHuesped.html', data)

def editarHuesped(request, id):
    data = {
        'usuarios': listar_users(),
        'habitaciones': listar_habitacion(),
        'empresas': listar_empresa(),
    }

    if request.method == 'POST':
        id_huesped = id
        usuario = int(request.POST.get('id_usuarios'))
        empresa = int(request.POST.get('id_empresas'))
        habitacion = int(request.POST.get('id_habitaciones'))
        salida = editar_huesped(id_huesped,usuario,empresa,habitacion)
        
        if salida == 1:
            data['mensaje'] = 'Modificado correctamente'
        else:
            data['mensaje'] = 'No se a podido registrar'

    return render(request, 'core/editar/huesped/editarHuesped.html', data)

def eliminarHuesped(request, id):
    data = {}

    if request.method == 'POST':
        id_huesped = id
        salida = eliminar_huesped(id_huesped)
        if salida == 1:
            data['mensaje'] = 'Eliminado con exito'
        else:
            data['mensaje'] = 'No se a podido eliminar'

    return render(request, 'core/editar/huesped/eliminarHuesped.html', data)

def registrohabitaciones(request):
    data = {
        'habitaciones' : listar_habitacion(),
    }
    
    return render(request, 'core/registrohabitaciones.html', data)

def crearHabitaciones(request):
    data = {}
    if request.method == 'POST':
        tipo_cama = request.POST.get('tipo_cama')
        precio_hab = int(request.POST.get('precio'))
        tamano = request.POST.get('tamano')
        cant_cama = int(request.POST.get('cant_cama'))
        disponi = request.POST.get('dispo')
        
        salida = agregar_habitacion(tipo_cama, precio_hab, tamano, cant_cama, disponi)

        if salida == 1:
            data['mensaje'] = 'registrado correctamente'
        else:
            data['mensaje'] = 'No se a podido registrar'

    return render(request, 'core/editar/habitaciones/crearHabitaciones.html', data)

def editarHabitaciones(request, id):
    data = {}

    if request.method == 'POST':
        id_habitaciones = id
        tipo_cama = request.POST.get('tipo_cama')
        precio_hab = int(request.POST.get('precio'))
        tamano = request.POST.get('tamano')
        cant_cama = int(request.POST.get('cant_cama'))
        disponi = request.POST.get('dispo')
        
        salida = editar_habitaciones(id_habitaciones,tipo_cama, precio_hab, tamano, cant_cama, disponi)
        if salida == 1:
            data['mensaje'] = 'Habitacion modificada correctamente'
        else:
            data['mensaje'] = 'No se a podido modificar'

    return render(request, 'core/editar/habitaciones/editarHabitaciones.html', data)

def eliminarHabitaciones(request, id):
    data = {}

    if request.method == 'POST':
        id_habitaciones = id
        salida = eliminar_habitaciones(id_habitaciones)
        if salida == 1:
            data['mensaje'] = 'Eliminado con exito'
        else:
            data['mensaje'] = 'No se a podido eliminar'

    return render(request, 'core/editar/habitaciones/eliminarHabitaciones.html', data)

def ordenpedido(request):
    return render(request, 'core/ordenpedido.html')

def registrocomedor(request):
    data = {
        'comedor' : listar_comidas(),
    }

    return render(request, 'core/registrocomedor.html', data)

def crearComedor(request):
    data = {}
    if request.method == 'POST':
        nom_plato = request.POST.get('nom_plato')
        precio_plato = int(request.POST.get('precio_plato'))
        tipo_plato = request.POST.get('tipo_plato')
        salida = agregar_comida(nom_plato,precio_plato,tipo_plato)
        if salida == 1:
            data['mensaje'] = 'registrado correctamente'
        else:
            data['mensaje'] = 'No se a podido registrar'

    return render(request, 'core/editar/comedor/crearComedor.html', data)

def editarComedor(request, id):
    data = {}

    if request.method == 'POST':
        id_comedor = id
        nom_plato = request.POST.get('nom_plato')
        precio_plato = int(request.POST.get('precio_plato'))
        tipo_plato = request.POST.get('tipo_plato')
        salida = editar_comida(id_comedor, nom_plato,precio_plato,tipo_plato)
        if salida == 1:
            data['mensaje'] = 'Platillo modificado correctamente'
        else:
            data['mensaje'] = 'No se a podido modificar'

    return render(request, 'core/editar/comedor/editarComedor.html', data)

def eliminarComedor(request, id):
    data = {}

    if request.method == 'POST':
        id_comedor = id
        salida = eliminar_comida(id_comedor)
        if salida == 1:
            data['mensaje'] = 'Eliminado con exito'
        else:
            data['mensaje'] = 'No se a podido eliminar'

    return render(request, 'core/editar/comedor/eliminarComedor.html', data)

def registroproductos(request):
    data = {
        'productos':listar_productos(),
    }

    return render(request, 'core/registroproductos.html', data)

def crearProductos(request):
    data = {
        'familia':listar_familia_producto(),
    }
    if request.method == 'POST':
        nombre_producto = request.POST.get('nombre_producto')
        fecha_vencimiento = request.POST.get('fecha_vencimiento')
        descripcion = request.POST.get('descripcion')
        id_familia = int(request.POST.get('id_familia'))
        cadena_vencimiento = re.sub('[\.-]','', fecha_vencimiento)
        codigo = int(str(request.POST.get('id_usuario'))+str(id_familia)+str(cadena_vencimiento))
        stock = int(request.POST.get('stocks'))
        salida = agregar_producto(nombre_producto,fecha_vencimiento,descripcion,id_familia,codigo,stock)
        if salida == 1:
            data['mensaje'] = 'registrado correctamente'
        else:
            data['mensaje'] = 'No se a podido registrar'

    return render(request, 'core/editar/productos/crearProductos.html', data)

def editarProductos(request, id):
    data = {'familia':listar_familia_producto(),}

    if request.method == 'POST':
        id_producto = id
        nombre_producto = request.POST.get('nombre_producto')
        fecha_vencimiento = request.POST.get('fecha_vencimiento')
        descripcion = request.POST.get('descripcion')
        id_familia = int(request.POST.get('id_familia'))
        cadena_vencimiento = re.sub('[\.-]','', fecha_vencimiento)
        codigo = int(str(request.POST.get('id_usuario'))+str(id_familia)+str(cadena_vencimiento))
        stock = int(request.POST.get('stocks'))
        salida = editar_producto(id_producto,nombre_producto,fecha_vencimiento,descripcion,id_familia,codigo,stock)
        if salida == 1:
            data['mensaje'] = 'Producto modificado correctamente'
        else:
            data['mensaje'] = 'No se a podido modificar'

    return render(request, 'core/editar/productos/editarProductos.html', data)

def eliminarProductos(request, id):
    data = {}

    if request.method == 'POST':
        id_producto = id
        salida = eliminar_producto(id_producto)
        if salida == 1:
            data['mensaje'] = 'Eliminado con exito'
        else:
            data['mensaje'] = 'No se a podido eliminar'

    return render(request, 'core/editar/productos/eliminarProductos.html', data)

def registrofamilias(request):
    data = {
        'familia':listar_familia_producto(),
    }

    return render(request, 'core/registrofamilias.html', data)

def crearFamilia(request):
    data = {}
    if request.method == 'POST':
        nombre_familia = request.POST.get('nombre_familia')

        salida = agregar_familia(nombre_familia)
        if salida == 1:
            data['mensaje'] = 'registrado correctamente'
        else:
            data['mensaje'] = 'No se a podido registrar'

    return render(request, 'core/editar/productos/crearFamilia.html', data)

def editarFamilia(request, id):
    data = {}

    if request.method == 'POST':
        id_familia = id
        nombre_familia = request.POST.get('nombre_familia')
        salida = editar_familia(id_familia,nombre_familia)
        if salida == 1:
            data['mensaje'] = 'Familia modificada correctamente'
        else:
            data['mensaje'] = 'No se a podido modificar'

    return render(request, 'core/editar/productos/editarFamilia.html', data)

def eliminarFamilia(request, id):
    data = {}

    if request.method == 'POST':
        id_familia = id
        salida = eliminar_familia(id_familia)
        if salida == 1:
            data['mensaje'] = 'Eliminado con exito'
        else:
            data['mensaje'] = 'No se a podido eliminar'

    return render(request, 'core/editar/productos/eliminarFamilia.html', data)

def registroproveedor(request):
    data = {}

    if request.method == 'POST':
        rut_prov = request.POST.get('rut')
        nom_prov_usuario = request.POST.get('correo')
        nom_prov = request.POST.get('nombre')
        ape_prov = request.POST.get('apellido')
        
        salida = agregar_proveedor(rut_prov,nom_prov_usuario, nom_prov, ape_prov)
        if salida == 1:
            data['mensaje'] = 'registrado correctamente'
        else:
            data['mensaje'] = 'No se a podido registrar'

    return render(request, 'core/registroproveedor.html', data)

def agregar_cliente(rut_cliente, nom_usuario, pass_usuario, nom_cliente, apellidos_cliente, num_tel, id_empresa):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_CREATE_CLIENTE', [rut_cliente, nom_usuario, pass_usuario, nom_cliente, apellidos_cliente, num_tel, id_empresa, salida])
    return salida.getvalue()

def listar_empresa():
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    out_cur = django_cursor.connection.cursor()

    cursor.callproc('SP_LISTAR_EMPRESA', [out_cur])

    lista = []
    for fila in out_cur:
        lista.append(fila)
    return lista

def agregar_proveedor(rut_prov, nom_prov_usuario, nom_prov, ape_prov):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_CREATE_PROVEEDOR', [rut_prov, nom_prov_usuario, nom_prov, ape_prov, salida])
    return salida.getvalue()

def agregar_habitacion(tipo_cama, precio_hab, tamano, cant_cama, disponi):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_CREATE_HABITACION', [tipo_cama, precio_hab, tamano, cant_cama, disponi, salida])
    return salida.getvalue()

def editar_habitaciones(id_habitaciones,tipo_cama, precio_hab, tamano, cant_cama, disponi):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_UPDATE_HABITACION', [id_habitaciones,tipo_cama, precio_hab, tamano, cant_cama, disponi, salida])
    return salida.getvalue()

def eliminar_habitaciones(id_habitaciones):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_DELETE_HABITACION', [id_habitaciones, salida])
    return salida.getvalue()

def agregar_comida(nom_plato,precio_plato,tipo_plato):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_CREATE_SERVICIO_COMEDOR', [nom_plato,precio_plato,tipo_plato, salida])
    return salida.getvalue()

def editar_comida(id_comedor,nom_plato,precio_plato,tipo_plato):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_UPDATE_SERVICIO_COMEDOR', [id_comedor,nom_plato,precio_plato,tipo_plato, salida])
    return salida.getvalue()

def eliminar_comida(id_comedor):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_DELETE_SERVICIO_COMEDOR', [id_comedor, salida])
    return salida.getvalue()

def agregar_factura(precio_hab,iva_19,precio_totale,nombre_empresa,nombre_cliente,habitacione,rut_cliente,fecha,creador):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_CREATE_FACTURA', [precio_hab,iva_19,precio_totale,nombre_empresa,nombre_cliente,habitacione,rut_cliente,fecha,creador, salida])
    return salida.getvalue()

def agregar_huesped(usuario,empresa,habitacion):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_CREATE_HUESPED', [usuario,empresa,habitacion, salida])
    return salida.getvalue()

def editar_huesped(id_huesped,usuario,empresa,habitacion):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_UPDATE_HUESPED', [id_huesped,usuario,empresa,habitacion, salida])
    return salida.getvalue()

def eliminar_huesped(id_huesped):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_DELETE_HUESPED', [id_huesped, salida])
    return salida.getvalue()

def agregar_producto(nombre_producto,fecha_vencimiento,descripcion,id_familia,codigo,stock):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_CREATE_PRODUCTOS', [nombre_producto,fecha_vencimiento,descripcion,id_familia,codigo,stock, salida])
    return salida.getvalue()

def editar_producto(id_producto,nombre_producto,fecha_vencimiento,descripcion,id_familia,codigo,stock):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_UPDATE_PRODUCTOS', [id_producto,nombre_producto,fecha_vencimiento,descripcion,id_familia,codigo,stock, salida])
    return salida.getvalue()

def eliminar_producto(id_producto):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_DELETE_PRODUCTOS', [id_producto, salida])
    return salida.getvalue()

def agregar_familia(nombre_familia):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_CREATE_FAMILIA_PRODUCTO', [nombre_familia, salida])
    return salida.getvalue()

def editar_familia(id_familia,nombre_familia):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_UPDATE_FAMILIA', [id_familia,nombre_familia, salida])
    return salida.getvalue()

def eliminar_familia(id_familia):
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    salida = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc('SP_DELETE_FAMILIA', [id_familia, salida])
    return salida.getvalue()

def listar_users():
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    out_cur = django_cursor.connection.cursor()
    cursor.callproc('SP_LISTAR_USUARIOS', [out_cur])
    lista = []
    for fila in out_cur:
        lista.append(fila)
    return lista

def listar_habitacion():
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    out_cur = django_cursor.connection.cursor()
    cursor.callproc('SP_LISTAR_HABITACION', [out_cur])
    lista = []
    for fila in out_cur:
        lista.append(fila)
    return lista

def listar_empresa():
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    out_cur = django_cursor.connection.cursor()
    cursor.callproc('SP_LISTAR_EMPRESAS', [out_cur])
    lista = []
    for fila in out_cur:
        lista.append(fila)
    return lista

def listar_comidas():
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    out_cur = django_cursor.connection.cursor()
    cursor.callproc('SP_LISTAR_SERVICIO_COMEDOR', [out_cur])
    lista = []
    for fila in out_cur:
        lista.append(fila)
    return lista

def listar_huesped():
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    out_cur = django_cursor.connection.cursor()
    cursor.callproc('SP_LISTAR_HUESPED', [out_cur])
    lista = []
    for fila in out_cur:
        lista.append(fila)
    return lista

def listar_factura():
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    out_cur = django_cursor.connection.cursor()
    cursor.callproc('SP_LISTAR_FACTURA', [out_cur])
    lista = []
    for fila in out_cur:
        lista.append(fila)
    return lista

def listar_productos():
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    out_cur = django_cursor.connection.cursor()
    cursor.callproc('SP_LISTAR_PRODUCTOS', [out_cur])
    lista = []
    for fila in out_cur:
        lista.append(fila)
    return lista

def listar_familia_producto():
    django_cursor = connection.cursor()
    cursor = django_cursor.connection.cursor()
    out_cur = django_cursor.connection.cursor()
    cursor.callproc('SP_LISTAR_FAMILIA_PRODUCTO', [out_cur])
    lista = []
    for fila in out_cur:
        lista.append(fila)
    return lista

def informe_habitaciones(self, *args, **kwargs):
    data = {
        'habitaciones' : listar_habitacion(),
    }
    pdf = render_to_pdf('core/editar/habitaciones/pdfHabitaciones.html', data)
    return HttpResponse(pdf, content_type='application/pdf')