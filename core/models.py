# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.contrib.auth.models import AbstractBaseUser


class Admin(models.Model):
    adm_rut = models.CharField(primary_key=True, max_length=10)
    adm_nom_usuario = models.CharField(max_length=30)
    adm_pass = models.CharField(max_length=30)
    adm_nom = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'admin'


class cargo(models.Model):
    nombre = models.CharField(max_length=20)
    descripcion = models.TextField()
    
    def __str__(self):
        return self.nombre


class Empresa(models.Model):
    id_empresa = models.AutoField(primary_key=True)
    nom_empresa = models.CharField(max_length=20)
    num_empresa = models.FloatField()
    direccion_empresa = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'empresa'


class Factura(models.Model):
    id_factura = models.AutoField(primary_key=True)
    precio_total = models.FloatField()
    nom_empresa = models.CharField(max_length=20, blank=True, null=True)
    nom_cliente_completo = models.CharField(max_length=50)
    cant_habitaciones = models.FloatField()
    gastos_comedor = models.FloatField()
    rut_cliente = models.ForeignKey(cargo, models.DO_NOTHING, db_column='rut_cliente')

    class Meta:
        managed = False
        db_table = 'factura'


class Habitacion(models.Model):
    id_habitacion = models.AutoField(primary_key=True)
    tipo_cama = models.CharField(max_length=10)
    precio_hab = models.FloatField()
    tamano = models.CharField(max_length=10)
    cant_cama = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'habitacion'


class OrdenPedido(models.Model):
    id_pedido = models.AutoField(primary_key=True)
    nom_prov = models.CharField(max_length=30)
    ape_prov = models.CharField(max_length=30)
    dia_pedido = models.DateField()
    cant_producto = models.FloatField()
    id_producto = models.ForeignKey('Productos', models.DO_NOTHING, db_column='id_producto')
    id_proveedor = models.ForeignKey('Proveedor', models.DO_NOTHING, db_column='id_proveedor')

    class Meta:
        managed = False
        db_table = 'orden_pedido'


class Productos(models.Model):
    id_producto = models.AutoField(primary_key=True)
    nom_producto = models.CharField(max_length=40)
    fecha_vencimiento = models.DateField()
    tipo_producto = models.CharField(max_length=50)
    precio = models.FloatField()

    class Meta:
        managed = False
        db_table = 'productos'


class Proveedor(models.Model):
    id_proveedor = models.AutoField(primary_key=True)
    rut_prov = models.CharField(max_length=10)
    nom_prov_usuario = models.CharField(max_length=30)
    pass_prov_usuario = models.CharField(max_length=30)
    nom_prov = models.CharField(max_length=30)
    ape_prov = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'proveedor'


class RecepcionPedido(models.Model):
    id_pedido = models.AutoField(primary_key=True)
    nom_proveedor = models.CharField(max_length=30)
    nom_tra = models.CharField(max_length=30)
    dia_recibido = models.DateField()
    cant_producto = models.FloatField()
    id_producto = models.ForeignKey(Productos, models.DO_NOTHING, db_column='id_producto')
    rut_tra = models.ForeignKey('Trabajador', models.DO_NOTHING, db_column='rut_tra')

    class Meta:
        managed = False
        db_table = 'recepcion_pedido'


class ServicioComedor(models.Model):
    id_plato = models.AutoField(primary_key=True)
    nom_plato = models.CharField(max_length=20)
    precio = models.FloatField()
    tipo_plato = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'servicio_comedor'


class Trabajador(models.Model):
    rut_tra = models.CharField(primary_key=True, max_length=10)
    nom_tra_usuario = models.CharField(max_length=20)
    pass_tra_usu = models.CharField(max_length=20)
    nom_tra = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'trabajador'
