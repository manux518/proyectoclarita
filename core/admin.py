from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(cargo)
admin.site.register(Empresa)
admin.site.register(Factura)
admin.site.register(Habitacion)
admin.site.register(OrdenPedido)
admin.site.register(Productos)
admin.site.register(Proveedor)
admin.site.register(RecepcionPedido)
admin.site.register(ServicioComedor)
admin.site.register(Trabajador)


