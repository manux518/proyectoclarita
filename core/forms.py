from django import forms
from django.db.models import fields
from .models import *
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User


class LoginForm(AuthenticationForm):
    pass

class registroform(UserCreationForm):
    class meta:
        model = User
        fields = ['email','username','first_name','last_name','password','password2']
