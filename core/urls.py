from django.urls import path
from.views import *


urlpatterns = [
    path('', home, name="home"),
    path('habitaciones/', habitaciones, name="habitaciones"),
    path ('promociones/', promociones, name='promociones'),
    path ('modulos/', modulos, name="modulos"),

#Ordenes De compras
    path ('ordenpedido/', ordenpedido, name='ordenpedido'),
    path ('ordencompra/', ordencompra, name='ordencompra'),

#Factura
    path('factura/<id>/', factura, name="factura"),
    path('crearFactura/<id>/', crearFactura, name="crearFactura"),
    path('listarfacturas/', listarfacturas, name="listarfacturas"),

#comedor
    path ('registrocomedor/', registrocomedor, name='registrocomedor'),
    path ('crearComedor/', crearComedor, name="crearComedor"),
    path ('editarComedor/<id>/', editarComedor, name="editarComedor"),
    path ('eliminarComedor/<id>/', eliminarComedor, name="eliminarComedor"),

#huespedes
    path ('registrohuesped/', registrohuesped, name='registrohuesped'),
    path ('crearHuesped/', crearHuesped, name="crearHuesped"),
    path ('editarHuesped/<id>/', editarHuesped, name="editarHuesped"),
    path ('eliminarHuesped/<id>/', eliminarHuesped, name="eliminarHuesped"),
    path ('registroproveedor/', registroproveedor, name='registroproveedor'),

#Habitaciones
    path ('registrohabitaciones/', registrohabitaciones, name='registrohabitaciones'),
    path ('crearHabitaciones/', crearHabitaciones, name="crearHabitaciones"),
    path ('editarHabitaciones/<id>/', editarHabitaciones, name="editarHabitaciones"),
    path ('eliminarHabitaciones/<id>/', eliminarHabitaciones, name="eliminarHabitaciones"),

#Productos
    path ('registroproductos/', registroproductos, name='registroproductos'),
    path ('registrofamilias/', registrofamilias, name='registrofamilias'),
    path ('crearProductos/', crearProductos, name='crearProductos'),
    path ('editarProductos/<id>/', editarProductos, name="editarProductos"),
    path ('eliminarProductos/<id>/', eliminarProductos, name="eliminarProductos"),
    path ('crearFamilia/', crearFamilia, name='crearFamilia'),
    path ('editarFamilia/<id>/', editarFamilia, name="editarFamilia"),
    path ('eliminarFamilia/<id>/', eliminarFamilia, name="eliminarFamilia"),

#login
    path ('modulos/', modulos, name="modulos"),
    path ('eliminarHabitaciones/<id>/', eliminarHabitaciones, name="eliminarHabitaciones"),
    
    path ('modulos/', modulos, name="modulos"),
    
    path('accounts/registrate/', registrate, name="registrate"),

#informes
    path('informe_habitaciones/', informe_habitaciones, name="informe_habitaciones"),


    path('usuario/<id>/', usuario , name='usuario'),
    path('nuevo_usuario/', nuevo_usuario, name='nuevo_usuario'),
    path('listar_usuario/',listar_usuario , name='listar_usuario'),
    path('modificar_usuario/<id>/',modificar_usuario,name='modificar_usuario'),
    path('eliminarusuario/<id>/',eliminar_usuario, name='eliminar_usuario'),
]